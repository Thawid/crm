<!-- start: Header -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="index.php"><span>OFFICE CRM</span></a>

            <!-- start: Header Menu -->
            <div class="nav-no-collapse header-nav">
                <ul class="nav pull-right">
				
                    <!-- start: User Dropdown -->
                    <li class="dropdown">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="halflings-icon white user"></i>
                            <?php
                            echo $_SESSION['NAME'] . " (" . $_SESSION['ADMIN'] . ")";
                            ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-menu-title">
                                <span>Account Settings</span>
                            </li>
							<?php
                    if ($_SESSION['ACCESS'] == true) {
                        ?>
                            <li><a href="change_pass.php"><i class="halflings-icon user"></i> CHANGE PASS</a></li>
					<?php } ?>
                            <li><a href="logout.php"><i class="halflings-icon off"></i> Logout</a></li>
                        </ul>
                    </li>
					
                    <!-- end: User Dropdown -->
                </ul>
            </div>
            <!-- end: Header Menu -->

        </div>
    </div>
</div>
<!-- start: Header -->
<div class="container-fluid-full">
    <div class="row-fluid">
        <!-- start: Main Menu -->
        <div id="sidebar-left" class="span2">
            <div class="nav-collapse sidebar-nav">
                <ul class="nav nav-tabs nav-stacked main-menu">
                    <li><a href="index.php"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
                    <li><a href="view_contract.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> View Contract</span></a></li>
					 <li><a href="client_list.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> View Client</span></a></li>
                    <li><a href="report_per.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Report By Percentage</span></a></li>
					<li><a href="report.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Report By Date</span></a></li>
                        <?php
                    if ($_SESSION['ACCESS'] == true) {
                        ?>
                         
                        
                        <li>
                            <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Setup</span></a>
                            <ul>
                                <li><a href="add_agent.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Add Agent</span></a></li>
                                
                            </ul>	
                        </li>
                        
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <!-- end: Main Menu -->