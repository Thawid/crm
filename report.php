<?php include('header_file.php'); ?>
<?php include('menu.php'); ?>
<?php include('date.php'); ?>

<div id="content" class="span10">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.php">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Add Agent</a></li>
    </ul>

    <div class="row-fluid">
        <div class="box span10">
            <div class="box-header" data-original-title="">
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Agent Details</h2>

            </div>
		<div class="box-content">
            <form  action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form-horizontal">
                <fieldset>

                    <div class="control-group">
                            <label class="control-label" for="inputSuccess">SELECT AGENT</label>
                            <div class="controls">
                                 <?php if ($_SESSION['ACCESS'] == true) { ?>
                                    <select name="agent_name" id="agent_name">
                                        <option value="0">SELECT ONE</option>
                                    <?php
                                    require_once 'class/control_view.php';
                                    $agent = view_agent();
                                    while ($get_agent = mysql_fetch_assoc($agent)) {
                                        echo "<option value=" . $get_agent['id'] . ">" . $get_agent['agent_name'] . "</option>";
                                    }
                                    ?>
                                    </select>
                                <?php } ?>
								 <?php
									require_once 'class/control_view.php';
									if ($_SESSION['ACCESS'] == false) {
									
								  ?>
								  <select name="agent_name" id="agent_name">
                                        <?php
											require_once 'class/control_view.php';
											$get_agent = view_agent_by_user($_SESSION['NAME']);
											
											
										 echo "<option value=" . $get_agent['id'] . ">" . $get_agent['agent_name'] . "</option>";
										?>
									</select>
									<?php }?>
                                <span class="help-inline"></span>
                            </div>
                        </div>
                    <div class="control-group">
                        <label class="control-label" for="datepicker">DATE </label>
                        <div class="controls">
                            <input type="text" name="date" id="datepicker">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="datepicker1">DATE </label>
                        <div class="controls">
                            <input type="text" name="datee" id="datepicker1">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="home_search" id="home_search" value="Search" class="btn btn-primary">Search</button>

                    </div>
                </fieldset>
            </form>
			</div>
		</div>

        <div class="row-fluid">

            <div class="box span12">
                <br/><br/><br/><br/> <br/><br/><br/><br/>
                <div class="box-header" data-original-title="">
                    <h2><i class="halflings-icon user"></i><span class="break"></span>ALL AGENT</h2>

                </div>
                <div class="box-content">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid"><div class="row-fluid"><div class="span6"><div class="dataTables_filter" id="DataTables_Table_0_filter"></div></div></div><table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 170px;">AGENT NAME</th>
                                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 248px;">PHONE</th>
                                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 248px;">WEB SITE</th>
                                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 248px;">DESCRIPTION</th>
                                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 248px;">PROGRESS</th>

                                </tr>
                            </thead>   

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                                <?php
                                $search = $_POST['agent_name'];
                                $date = $_POST['date'];
                                $datee = $_POST['datee'];
                                $table = "add_contract";
                                if ($search && $date && $datee) {
                                    $sql = "SELECT * from $table WHERE date BETWEEN '$date' AND '$datee' && agent_id = '$search'";
                                    $result = mysql_query($sql) or die(mysql_error());
                                    while ($section = mysql_fetch_array($result)) {
                                        ?>
                                        <tr class="odd">
                                            <td class=" sorting_1"><?php echo $section['name'] ?></td>
                                            <td class="center "><?php echo $section['phone'] ?></td>
                                            <td class="center "><?php echo $section['website'] ?></td>
                                            <td class="center "><?php echo $section['description'] ?></td>
                                            <td class="center "><?php echo $section['progress'] ?></td>
                                        </tr>

                                    <?php } ?>
                                    <?php
                                } elseif ($search) {
                                    $sql = "SELECT * from $table WHERE agent_id = '$search'";
                                    $result = mysql_query($sql) or die(mysql_error());
                                    while ($section = mysql_fetch_array($result)) {
                                        ?>
                                        <tr class="odd">
                                            <td class=" sorting_1"><?php echo $section['name'] ?></td>
                                            <td class="center "><?php echo $section['phone'] ?></td>
                                            <td class="center "><?php echo $section['website'] ?></td>
                                            <td class="center "><?php echo $section['description'] ?></td>
                                            <td class="center "><?php echo $section['progress'] ?></td>

                                        </tr>

                                    <?php } ?>
                                    <?php
                                } else {
                                    echo 'Not Found';
                                }
                                ?>


                            </tbody></table><div class="row-fluid"></div>            
                    </div>
                </div>
            </div>


        </div>


    </div>	

</div>

<script type="text/javascript">
    function valid_num(value) {
        $.get(
                'ajax.valid.php',
                {action: "phone_valid", number: value},
                function (data) {
                    $("#get_result").html(data);
                }
        )
    }
    function agent_status(value) {
        $.get(
                'ag_status.php',
                {status: value},
                function (data) {
                    $("#agent_status" + value).html(data);
                }
        )
    }
</script>



<?php include ('footer.php') ?>
