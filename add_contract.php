<?php include('header_file.php'); ?>
<?php include('date.php'); ?>
<?php include('menu.php'); ?>

<div id="content" class="span10">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.php">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Add Contract</a></li>
    </ul>

    <div class="row-fluid">
        <div class="box span10">
            <div class="box-content">
                <?php
                if ($_SERVER['REQUEST_METHOD'] == "POST") {
                    require_once 'class/control_valid.php';
                    $agent_id = $_POST['agent_id'];
                    $name = $_POST['name'];
                    $phone = $_POST['phone'];
                    $website = $_POST['website'];
                    $description = $_POST['description'];
                    $progress = $_POST['progress'];
                    $date = $_POST['date'];

                   $result = valid_contract($phone);
                    if ($result == true) {
                        require_once 'class/control_register.php';
                        $get_result = register_contract($agent_id, $name, $phone, $website, $description, $progress, $date);
                        if ($get_result == true) {
                            ?>
                            <ul class="tickets metro">
                                <li class="ticket blue">
                                    <a href="#">
                                        <span class="content">
                                            <span class="status">Status: [ Complete ]</span>
                                        </span>	                                                       
                                    </a>
                                </li>
                            </ul>
                        <?php } else { ?>
                            <ul class="tickets metro">
                                <li class="ticket red">
                                    <a href="#">
                                        <span class="content">
                                            <span class="status">Status: [ Fail ]</span>
                                        </span>	                                                       
                                    </a>
                                </li>
                            </ul>

                        <?php
                        }
                    } else {
                        ?>
                        <ul class="tickets metro">
                            <li class="ticket red">
                                <a href="#">
                                    <span class="content">
                                        <span class="status">Status: [ Phone Number Should Be Unique.. ]</span>
                                    </span>	                                                       
                                </a>
                            </li>
                        </ul>
                        <?php
                } 
                
                    }
                ?>
            </div>
            <div class="box-header" data-original-title="">
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Contract Details</h2>
                <div class="box-icon">
                    <a href="view_contract.php" class="btn btn-primary"><i class="halflings-icon fast-backward"></i>GO BACK</a>

                </div>
            </div>
            <div class="box-content">
                <form  action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST" class="form-horizontal">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="inputSuccess">SELECT AGENT</label>
                            <div class="controls">
                                 <?php if ($_SESSION['ACCESS'] == true) { ?>
                                    <select name="agent_id" id="agent_id">
                                        <option value="0">SELECT ONE</option>
                                    <?php
                                    require_once 'class/control_view.php';
                                    $agent = view_agent();
                                    while ($get_agent = mysql_fetch_assoc($agent)) {
                                        echo "<option value=" . $get_agent['id'] . ">" . $get_agent['agent_name'] . "</option>";
                                    }
                                    ?>
                                    </select>
                                <?php } ?>
								 <?php
									require_once 'class/control_view.php';
									if ($_SESSION['ACCESS'] == false) {
									
								  ?>
								  <select name="agent_id" id="agent_id">
                                        <?php
											require_once 'class/control_view.php';
											$get_agent = view_agent_by_user($_SESSION['NAME']);
											
											
										 echo "<option value=" . $get_agent['id'] . ">" . $get_agent['agent_name'] . "</option>";
										?>
									</select>
									<?php }?>
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputSuccess">NAME</label>
                            <div class="controls">
                                <input type="text" name="name" id="nmae">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="control-group success">
							<label class="control-label" for="inputSuccess">PHONE NUMBER</label>
							<div class="controls">
								<input type="text" name="phone" id="phone">
							</div>
                         </div>
                        <div class="control-group">
                            <label class="control-label" for="inputSuccess">WEB SITE/E-MAIL</label>
                            <div class="controls">
                                <input type="text" name="website" id="website">
                                
                            </div>
                        </div>
                         <div class="control-group">
                            <label class="control-label" for="datepicker">DATE </label>
							<?php if ($_SESSION['ACCESS'] == true) { ?>
                            <div class="controls">
                                <input type="text" name="date" id="datepicker" >
                            </div>
							<?php }?>
							<?php if ($_SESSION['ACCESS'] == false) { ?>
                            <div class="controls">
                                <input type="text" name="date" id="date" value="<?php echo date("m/d/Y");?>">
                            </div>
							<?php }?>
						</div>
                           <div class="control-group">
                            <label class="control-label" for="inputSuccess">PROGRESS</label>
                            <div class="controls">
                                <input type="text" name="progress" id="progress" placeholder="100%">
                                <span class="help-inline"></span>
                            </div>
                           </div>
                        <div class="control-group hidden-phone">
                            <label class="control-label" for="textarea2">DESCRIPTION</label>
                            <div class="controls">
                                <div class="cleditorMain" style="width: 500px; height: 250px;">
                                    <textarea class="cleditor" name="description" id="detail" rows="3" style="display: none; width: 500px; height: 197px;">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                       
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <button class="btn">Cancel</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>	

    <script type="text/javascript">
    function valid_num(value) {
        $.get(
                'ajax.valid_contract.php',
                {action: "phone_valid", number: value},
                function (data) {
                    $("#get_result").html(data);
                }
        )
    }
    function agent_status(value) {
        $.get(
                'ag_status.php',
                {status: value},
                function (data) {
                    $("#agent_status" + value).html(data);
                }
        )
    }
</script>	
  
<?php include ('footer.php') ?>