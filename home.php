<?php include('header_file.php'); ?>
<?php include('menu.php'); ?>

<!-- start: Content -->

<div id="content" class="span10">
<div class="row-fluid">
<ul class="breadcrumb">
<li>
<i class="icon-home"></i>
<a href="index.php">Home</a> 
<i class="icon-angle-right"></i>
</li>
<li><a href="#">Dashboard</a></li>
<li STYLE="margin-left: 125px"><label class="control-label" for="inputSuccess"><span STYLE="font-size: large; color: #4cae4c">PHONE NUMBER</span></label></li>
<li>
	<div class="control-group success">
		
		<div class="controls">
			<input onkeyup="valid_num(this.value)" type="text" name="phone_num" id="phone_num">
			<span id="get_result" class="help-inline"></span>
		</div>
	</div>
</li>
</ul>

<div class="row-fluid">

<div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
<div class="boxchart">5,6,7,2,0,4,2,4,8,2,3,3,2</div>
<div class="number">854<i class="icon-arrow-up"></i></div>
<div class="title">visits</div>
<div class="footer">
<a href="#"> read full report</a>
</div>	
</div>
<div class="span3 statbox green" onTablet="span6" onDesktop="span3">
<div class="boxchart">1,2,6,4,0,8,2,4,5,3,1,7,5</div>
<div class="number">123<i class="icon-arrow-up"></i></div>
<div class="title">sales</div>
<div class="footer">
<a href="#"> read full report</a>
</div>
</div>
<div class="span3 statbox blue noMargin" onTablet="span6" onDesktop="span3">
<div class="boxchart">5,6,7,2,0,-4,-2,4,8,2,3,3,2</div>
<div class="number">982<i class="icon-arrow-up"></i></div>
<div class="title">orders</div>
<div class="footer">
<a href="#"> read full report</a>
</div>
</div>
<div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
<div class="boxchart">7,2,2,2,1,-4,-2,4,8,,0,3,3,5</div>
<div class="number">678<i class="icon-arrow-down"></i></div>
<div class="title">visits</div>
<div class="footer">
<a href="#"> read full report</a>
</div>
</div>	
<div class="box span12">
<div class="box span12">
<div class="box-header" data-original-title="">
<h2><i class="halflings-icon user"></i><span class="break"></span>CONTRACT LIST BY CURRENT DATE</h2>
<div class="box-icon">
<a class="btn btn-success" href="add_contract.php">ADD CONTRACT</a>
</div>

</div>

<div class="box-content">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid"><div class="row-fluid"><div class="span6"><div class="dataTables_filter" id="DataTables_Table_0_filter"></div></div></div><table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
	<thead>
		<tr role="row">
			<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 170px;">AGENT NAME</th>
			<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 170px;">NAME</th>
			<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 248px;">PHONE</th>
			<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 136px;">WEB SITE/E-MAIL</th>
			<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 136px;">DETAILS</th>
			<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 136px;">PROGRESS</th>
			<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 136px;">DATE</th>

		</tr>
	</thead>   

	<tbody id="getinfo" role="alert" aria-live="polite" aria-relevant="all">
	<?php
	require_once 'class/control_view.php';
	if ($_SESSION['ACCESS'] == true) {
		$get_info = view_contract_by_date();
	}

	if ($_SESSION['ACCESS'] == false) {
		$get_info = view_contract_by_agent_by_date($_SESSION['ID']);
	}
	
	while ($row = mysql_fetch_array($get_info)) {
		//echo "<pre>";
		//print_r($row);
		//exit;
		?>

			<tr class="odd">
				<td class=" sorting_1"><?php
					$agent_id = $row[1];
					$show = view_agent_by_agent_id($agent_id);
					echo $show['agent_name'];
					?>
				</td>
				<td class="center"><?php echo $row['name'] ?></td>
				<td class="center"><?php echo $row['phone'] ?></td>
				<td class="center "><?php echo $row['website'] ?></td>
				<td class="center "><?php echo $row['description'] ?></td>
				<td class="center "><?php echo $row['progress'] ?></td>
				<td class="center "><?php echo $row['date'] ?></td>

				<?php
			}
			?>
		</tr>

	</tbody></table><div class="row-fluid"></div>            
</div>
</div>
</div>

</div>
</div>
</div>
<script type="text/javascript">
function valid_num(value) {
$.get(
'ajax.valid_contract.php',
{action: "phone_valid", number: value},
function (data) {
$("#get_result").html(data);
}
)
}
function agent_status(value) {
$.get(
'ag_status.php',
{status: value},
function (data) {
$("#agent_status" + value).html(data);
}
)
}
</script>
<?php include ('footer.php') ?>
