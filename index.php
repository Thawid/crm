<?php
	session_start();
	if(isset($_SESSION['ADMIN'])){
		header('Location: home.php');
		exit();
	}
	if($_SERVER['REQUEST_METHOD']=="POST"){
		require_once('class/config.php'); // This is mysql connection
		$agent_name = $_POST['agent_name'];
		$password = sha1($_POST['password']);
		$sql = "SELECT * FROM add_agent WHERE agent_name = '$agent_name' AND password = '$password' AND status = 1";
		$query = mysql_query($sql);
		$results =  mysql_num_rows($query);
		if($results == TRUE){								
			while($row = mysql_fetch_assoc($query)){
				$_SESSION['ADMIN'] = "AGENT";
				$_SESSION['ID'] =	$row['id'];
				$_SESSION['NAME'] =	$row['agent_name'];
				$_SESSION['PHONE'] = $row['phone'];
				$_SESSION['UID'] = $row['unique_id'];
				$_SESSION['ACCESS'] = false;
				if(isset($_SESSION['ADMIN'])){
					header('Location: home.php');
				}
			}
		}else{ 
			$message = '<p style="color: rgb(255, 0, 0);">Invalid User Name or Password</p>';
		}
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>OFFICE CRM</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	
  <link rel="stylesheet" href="css/remodal.css">
  <link rel="stylesheet" href="css/remodal-default-theme.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	
			<style type="text/css">
			body { background: url(img/bg-login.jpg) !important; }
		</style>
		
		
		
</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
				<div class="login-box">
					<div class="icons">
						  <a href="#modal" class="btn">
                             <i class="halflings-icon home"></i>
                          </a>
                          
					<h1 class="text-center text-success">AGENT LOGIN</h1> <br />
					<h2 class="text-center">Login to your account</h2> <br /> 
					<div class="text-center"> 
						<?php
						if(isset($message)){
							echo $message;
						}
						?>
					</div>
					<form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
						<fieldset>
							
							<div class="input-prepend" title="agent name">
								<span class="add-on"><i class="halflings-icon user"></i></span>
								<input class="input-large span10" name="agent_name" id="agent_name" type="text" placeholder="type agent name"/>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
							</div>
							<div class="clearfix"></div>
							
							<label class="remember" for="remember">
								<input style="margin-top: 0px;" type="checkbox" id="remember" />
								<span class="text-center">Remember me</span>	
							</label>

							<div class="button-login">	
								<input class="btn btn-primary" type="submit" value="Login" />
							</div>
							<div class="clearfix"></div>
					</form>
					<hr>
					
				</div><!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
		</div><!--/fluid-row-->
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
		<!--<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

		<script src="js/jquery.ui.touch-punch.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>


		<script src="js/jquery.uniform.min.js"></script>

		<script src="js/jquery.cleditor.min.js"></script>
		<script src="js/custom.js"></script>-->
		<script type="text/javascript" src="js/remodal.js"></script>
	<!-- Events -->
<script>
  //  The second way to initialize:
  $('[data-remodal-id=modal2]').remodal({
    modifier: 'with-red-theme'
  });
</script>
<script type="text/javascript">

function showDistrict(str) {
	
    if (str == 0) {
    	 document.getElementById("control_district").innerHTML = ' <select class="text-center form-control" disabled="1" name="district_id"  id="district_id"><option value="0">SELECT DISTRICT</option></select>';
        document.getElementById("control_sub_district").innerHTML = '<select class="text-center form-control" disabled="1" name="sub_district_id"  id="sub_district_id"><option value="0">SELECT UPZELA</option></select>';
      
        return;
    }else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("control_district").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET","ajax_district.php?q="+str,true);
        xmlhttp.send();
    }
}
function sub_dis(value){
	 if (value == 0) {
	 	$("#sub_district_id").attr("width","500");
	 	return;
	 }else{
	$.get("ajax_sub_districtIndex.php",
		{dis_id:value},
		function(data){			
			$("#control_sub_district").html(data);
		}
	)}
}
</script>
<script type="text/javascript">
function setBranch(value){
	$.get('ajax_branch.php',
		{sub_dis_id:value},
		function(data){			
			$("#control_branch").html(data);
		}
	)
}
function getDetail(value){
	$.get('ajax.getDetail.php',
		{branch:value},
		function(data){			
			$("#get_details").html(data);
		}
	)
}
</script>
	<!-- end: JavaScript-->
		
</body>
</html>
