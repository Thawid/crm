<?php include('header_file.php'); ?>
<?php include('menu.php'); ?>
<?php include('date.php'); ?>
<?php
if ($_SESSION['ACCESS'] == false) {
    header('location: home.php');
    exit();
}
?>
<?php $up_id = $_GET['up_id']; ?>
<?php include('class/control_view.php'); ?>
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.php">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Update Contract</a></li>
    </ul>

    <div class="row-fluid">
        <div class="box span10">
            <div class="box-content">
                <?php
                if ($_SERVER['REQUEST_METHOD'] == "POST") {
                    require_once('class/control_update.php');
                    require_once('class/control_valid.php');
                    $agent_id = $_POST['agent_id'];
                    $name = $_POST['name'];
                    $phone = $_POST['phone'];
                    $website = $_POST['website'];
                     $date = $_POST['date'];
                    $result = update_contract($up_id,$agent_id,$name,$phone,$website,$date);
                    if ($result == true) {
                        ?>
                        <ul class="tickets metro">
                            <li class="ticket blue">
                                <a href="#">
                                    <span class="content">
                                        <span class="status">Status: [ Update Success ]</span>
                                    </span>	                                                       
                                </a>
                            </li>
                        </ul>
                    <?php } else { ?>
                        <ul class="tickets metro">
                            <li class="ticket red">
                                <a href="#">
                                    <span class="content">
                                        <span class="status">Status: [ Update Fail ]</span>
                                    </span>	                                                       
                                </a>
                            </li>
                        </ul>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="box-header" data-original-title="">
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Update Contract</h2>
                <div class="box-icon">
                    <a href="view_contract.php" class="btn btn-primary"><i class="halflings-icon fast-backward"></i>GOO BACK</a>

                </div>
            </div>
            <div class="box-content">
               
                <form class="form-horizontal" method="POST">
                    <fieldset>
                        <div class="control-group success">
                            <label class="control-label" for="inputSuccess">AGENT NAME</label>
                            <select name="agent_id" id="agent_id">
                                <option value="<?php echo $_GET['id'] ?>">
                                    <?php echo $_GET['agent_name'] ?>
                                </option>                         
                                <?php
                                $option_view = view_contract_by_agent_id($_GET['agent_id']);
                                while ($row_op = mysql_fetch_assoc($option_view)) {
                                    ?>
                                    <option value="<?php echo $row_op['id']; ?>">
                                    <?php echo $row_op['agent_name']; ?>
                                    </option>
                                <?php }
                                ?>
                            </select>
                        </div>

                        <div class="control-group success">
                            <label class="control-label" for="inputSuccess">CLIENT NAME</label>
                            <div class="controls">
                                 <?php $get_info = view_contract_by_id($up_id); ?>
                                <input type="text" id="name" name="name" value="<?php echo $get_info['name']; ?>">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="control-group success">
                            <label class="control-label" for="inputSuccess">PHONE</label>
                            <div class="controls">
                                <input type="text" id="phone" name="phone" value="<?php echo $get_info['phone']; ?>">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="control-group success">
                            <label class="control-label" for="inputSuccess">WEB SITE</label>
                            <div class="controls">
                                <input type="text" id="website" name="website" value="<?php echo $get_info['website']; ?>">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="control-group success">
                            <label class="control-label" for="datepicker">DATE</label>
                            <div class="controls">
                                <input type="text" id="datepicker" name="date" value="<?php echo $get_info['date']; ?>">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <button class="btn">Cancel</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>




<?php include ('footer.php') ?>