<?php include('header_file.php'); ?>
<?php include('menu.php'); ?>
<?php
if ($_SESSION['ACCESS'] == false) {
header('location: home.php');
exit();
}
?>
<div id="content" class="span10">
<ul class="breadcrumb">
<li>
	<i class="icon-home"></i>
	<a href="index.php">Home</a> 
	<i class="icon-angle-right"></i>
</li>
<li><a href="#">Add Agent</a></li>
</ul>

<div class="row-fluid">
<div class="box span10">
	<div class="box-header" data-original-title="">
		<h2><i class="halflings-icon edit"></i><span class="break"></span>Agent Details</h2>

	</div>
	<div class="box-content">
		<?php
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			require_once 'class/control_register.php';
			require_once 'class/control_valid.php';
			$agent_name = $_POST['agent_name'];
			$phone_num = $_POST['phone_num'];
			$password = $_POST['password'];
			$uid = $_POST['uid'];
			$password = SHA1($password);
			$agent_valid = valid_agent($phone_num);
			if ($agent_valid == true) {
				$get_info = register_agent($agent_name, $phone_num, $password, $uid);
				if ($get_info == true) {
					?>
					<ul class="tickets metro">
						<li class="ticket blue">
							<a href="#">
								<span class="content">
									<span class="status">Status: [ Complete ]</span>
								</span>	                                                       
							</a>
						</li>
					</ul>
				<?php } else {
					?>
					<ul class="tickets metro">
						<li class="ticket red">
							<a href="#">
								<span class="content">
									<span class="status">Status: [ Fail ]</span>
								</span>	                                                       
							</a>
						</li>
					</ul>
				<?php
				}
			} else {
				?>
				<ul class="tickets metro">
					<li class="ticket red">
						<a href="#">
							<span class="content">
								<span class="status">Status: [ Already Inserted ]</span>
							</span>	                                                       
						</a>
					</li>
				</ul>
			<?php
			}
		}
		?>
	</div>
	<form  action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST" class="form-horizontal">
		<fieldset>
			<div class="control-group success">
				<label class="control-label" for="inputSuccess">AGENT NAME</label>
				<div class="controls">
					<input type="text" name="agent_name" id="agent_name">
					<span class="help-inline"></span>
				</div>
			</div>
			<div class="control-group success">
				<label class="control-label" for="inputSuccess">PHONE NUMBER</label>
				<div class="controls">
					<input onkeyup="valid_num(this.value)" type="text" name="phone_num" id="phone_num">
					<span id="get_result" class="help-inline"></span>
				</div>
			</div>
			<div class="control-group success">
				<label class="control-label" for="inputSuccess">PASSWORD</label>
				<div class="controls">
					<input type="password" name="password" id="password">
					<span class="help-inline"></span>
				</div>
			</div>
			<div class="control-group success">
				<label class="control-label" for="inputSuccess">UID</label>
				<div class="controls">
					<select name="uid" id="uid">
						<option value="0">SELECT UID</option>
						<?php
						require_once 'class/control_view.php';
						$get_uid = view_item();
						while ($get_row = mysql_fetch_assoc($get_uid)) {
							?>
							<option value="<?php echo $get_row['u_id'] ?>"><?php echo $get_row['u_id'] ?></option>	
						<?php }
						?>
					</select>
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" class="btn btn-primary">Save changes</button>
				<input type="reset" value="Cancel" class="btn btn-danger">
			</div>
		</fieldset>
	</form>


	
</div>
<div class="row-fluid">
	<div class="box span12">
		<div class="box span12">
			<div class="box-header" data-original-title="">
				<h2><i class="halflings-icon user"></i><span class="break"></span>ALL AGENT</h2>

			</div>
			<div class="box-content">
				<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid"><div class="row-fluid"><div class="span6"><div class="dataTables_filter" id="DataTables_Table_0_filter"></div></div></div><table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
						<thead>
							<tr role="row">
								<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 170px;">AGENT NAME</th>
								<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 248px;">PHONE</th>

								<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 145px;">Status</th>
								<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 289px;">Actions</th>
							</tr>
						</thead>   

						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							require_once 'class/control_view.php';
							$display_agent = view_agent();
							while ($view = mysql_fetch_assoc($display_agent)) {
								?>

								<tr class="odd">
								
									<td class=" sorting_1"><?php echo $view['agent_name'] ?></td>
									<td class="center "><?php echo $view['phone'] ?></td>

									<td class="center ">
										<div id="agent_status<?php echo $view['id'] ?>">
											<?php if ($view['status'] == 1) { ?>
												<span class='label label-success'>Active</span>
												<a style='float:right' class='label label-danger' onclick="agent_status(<?php echo $view['id'] ?>)" >Disable</a>
											<?php } else { ?>
												<span class='label label-danger'>Disable</span>
												<a style='float:right' class='label label-success' onclick="agent_status(<?php echo $view['id'] ?>)" >Active</a>
											<?php }
										?>	
										</div>
									</td>
									<td class="center ">
										<a class="btn btn-info" href="update_agent.php?update=<?php echo $view['id'] ?>">
											<i class="halflings-icon white edit"></i>  
										</a>
										<!--<a class="btn btn-danger" href="#">
											<i class="halflings-icon white trash"></i> 
										</a>-->
									</td>
								</tr>

						<?php }
						?>
						</tbody></table><div class="row-fluid"></div>            
				</div>
			</div>
		</div>

	</div>
</div>


</div>	

</div>

<script type="text/javascript">
function valid_num(value) {
$.get(
		'ajax.valid.php',
		{action: "phone_valid", number: value},
		function (data) {
			$("#get_result").html(data);
		}
)
}
function agent_status(value) {
$.get(
		'ag_status.php',
		{status: value},
		function (data) {
			$("#agent_status" + value).html(data);
		}
)
}
</script>



<?php include ('footer.php') ?>
