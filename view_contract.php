<?php include('header_file.php'); ?>
<?php include('menu.php'); ?>
<?php require_once("class/config.php"); ?>
<link rel="stylesheet" href="css/remodal.css">
<link rel="stylesheet" href="css/remodal-default-theme.css">

<?php
$tbl_name = "add_contract";
if(isset($_POST['checkbox'])){$checkbox = $_POST['checkbox'];
if(isset($_POST['activate'])?$activate = $_POST["activate"]:$deactivate = $_POST["deactivate"])
$id = "('" . implode( "','", $checkbox ) . "');" ;
$sql="UPDATE add_contract SET status = '".(isset($activate)?'1':'0')."' WHERE id IN $id" ;
$result = mysql_query($sql) or die(mysql_error());
}
?>
<script>
function showEdit(editableObj) {
$(editableObj).css("background", "#A4A4A4");
}

function saveToDatabase(editableObj, column, id) {
$(editableObj).css("background", "#FFF url(loaderIcon.gif) no-repeat right");
$.ajax({
	url: "land_drop.php",
	type: "POST",
	data: 'column=' + column + '&editval=' + editableObj.innerHTML + '&id=' + id,
	success: function (data) {
		$(editableObj).css("background", "#FDFDFD");
	}
});
}
</script>
<div id="content" class="span12">
<ul class="breadcrumb">
<li>
	<i class="icon-home"></i>
	<a href="index.php">Home</a> 
	<i class="icon-angle-right"></i>
</li>
<li><a href="#">All Contract</a></li>
</ul>

<div class="row-fluid">
<div class="box span12">
	<div class="box span12">
		<div class="box-header" data-original-title="">
			<h2><i class="halflings-icon user"></i><span class="break"></span>CONTRACT LIST</h2>
			<div class="box-icon">
				<a class="btn btn-success" href="add_contract.php">ADD CONTRACT</a>
			</div>

		</div>

		<div class="box-content">
			<form name="frmactive" method="post" action="">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid"><div class="row-fluid"><div class="span6"><div class="dataTables_filter" id="DataTables_Table_0_filter"></div></div></div><table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
					<thead>
					<tr role="row">
						<td colspan="5">
						<input name="deactivate" type="submit" id="deactivate" value="CLIENT" />
						</td>
					</tr>
						<tr role="row">
							<td align="center"><input type="checkbox" name="allbox" title="Select or Deselct ALL" style="background-color:#ccc;"/></td>
							<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 170px;">AGENT NAME</th>
							<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 170px;">NAME</th>
							<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 248px;">PHONE</th>
							<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 136px;">WEB SITE/E-MAIL</th>
							<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 136px;">DESCRIPTION</th>
							<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 136px;">PROGRESS</th>
							<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 136px;">DATE</th>
							<?php
							if ($_SESSION['ACCESS'] == true) {
								?>
								<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 289px;">Actions</th>
								<?php
							}
							?>
						</tr>
					</thead>   

					<tbody id="getinfo" role="alert" aria-live="polite" aria-relevant="all">
						<?php
						require_once 'class/control_view.php';
						if ($_SESSION['ACCESS'] == true) {
							$get_info = view_contract();
						}

						if ($_SESSION['ACCESS'] == false) {
							$get_info = view_contract_by_agent($_SESSION['ID']);
						}
						$count=mysql_num_rows($get_info);
						while ($row = mysql_fetch_array($get_info, MYSQL_NUM)) {
							$k = $row[0];
							//echo "<pre>";
							//print_r($row);
							//exit;
							//foreach($row as $k=>$v) {
							?>

							<tr class="odd">
								<td align="center"><input name="checkbox[]" type="checkbox" id="checkbox[]" value="<?php echo 

$row[0] ?>"></td>
								<td class=" sorting_1"><?php
									$agent_id = $row[1];
									$show = view_agent_by_agent_id($agent_id);
									echo $show[1];
									?></td>
								<td class="center"><?php echo $row[2] ?></td>
								<td class="center"><?php echo $row[3] ?></td>
								<td class="center "><?php echo $row[4] ?></td>
								<td contenteditable="true" onBlur="saveToDatabase(this, 'description', '<?php echo $row[0]; ?>')" onClick="showEdit(this);"><?php echo $row[5]; ?></td>
								<td contenteditable="true" onBlur="saveToDatabase(this, 'progress', '<?php echo $row[0]; ?>')" onClick="showEdit(this);"><?php echo $row[6]; ?></td>
								<td class="center "><?php echo $row[7] ?></td>
								<?php
								if ($_SESSION['ACCESS'] == true) {
									?>
									<td class="center ">
										<a class="btn btn-info" href="update_contract.php?up_id=<?php echo $row[0]; ?>&&agent_id=<?php echo $row[1]; ?>&&agent_name=<?php echo $show[1]; ?>">
											<i class="halflings-icon white edit"></i>  
										</a>
										<a href="#modal" class="btn btn-danger">
											<i class="halflings-icon white trash"></i> 

										</a>
										<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
											<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
											<div>
												<h3 id="modal1Desc">
													Are You Want to Delete it
												</h3>
											</div>
											<br>
											<button data-remodal-action="cancel" class="btn btn-info">Cancel</button>
											<button onclick="drop_contract(<?php echo $row[0]; ?>)" data-remodal-action="cancel" class="btn btn-danger">Delete</button>
										</div>

									</td>
									<?php
								}
								?>
							</tr>
							<?php //}
						}
						?>
					</tbody></table><div class="row-fluid"></div>            
			</div>
			</form>
		</div>
	</div>

</div>
</div>
<script type="text/javascript" src="js/remodal.js"></script>
<!-- Events -->
<script>
//  The second way to initialize:
$('[data-remodal-id=modal2]').remodal({
	modifier: 'with-red-theme'
});
</script>
<script type="text/javascript">
function statusFn(value) {
	$.get(
			'land_status.php',
			{ld_id: value},
			function (data) {
				$("#status" + value).html(data);
			}
	)
}
function drop_contract(value) {
	$.get(
			'land_drop.php',
			{ld: value},
			function (data) {
				$("#getinfo").html(data);
				;
			}
	)
}
</script>
<?php include ('footer.php') ?>
