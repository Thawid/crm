<?php include('header_file.php'); ?>
<?php include('menu.php'); ?>
<?php
	if($_SESSION['ACCESS'] == false){
		header('location: home.php');
		exit();
	}
?>
<div id="content" class="span10">
   <ul class="breadcrumb">
       <li>
           <i class="icon-home"></i>
           <a href="index.php">Home</a> 
           <i class="icon-angle-right"></i>
       </li>
       <li><a href="#">Update Agent</a></li>
   </ul>

   <div class="row-fluid">
       <div class="box span10">	
       <div class="box-content">
       		<?php
				$up_id = $_GET['update'];
				if($_SERVER['REQUEST_METHOD']=="POST"){
					require_once('class/control_update.php');
					require_once('class/control_valid.php');
					$up_name  = $_POST['up_name'];
					$up_phone = $_POST['up_phone'];
					$get_valid = valid_agent($up_phone);
					if($get_valid == true){
						$get_info = update_agent($up_id,$up_name,$up_phone);
						if($get_info == true){ ?>
							<ul class="tickets metro">
								<li class="ticket blue">
									<a href="#">
										<span class="content">
										<span class="status">Status: [ Update Success ]</span>
										</span>	                                                       
									</a>
								</li>
							</ul>
					<?php	}else{  ?>
							<ul class="tickets metro">
								<li class="ticket red">
									<a href="#">
										<span class="content">
										<span class="status">Status: [ Update Fail ]</span>
										</span>	                                                       
									</a>
								</li>
							</ul>
					<?php	}
					}else{ ?>
						<ul class="tickets metro">
								<li class="ticket red">
									<a href="#">
										<span class="content">
										<span class="status">Status: [ Phone Number Already used ]</span>
										</span>	                                                       
									</a>
								</li>
							</ul>
				<?php	}
					
				}
			?>
       </div>		
           <div class="box-header" data-original-title="">
               <h2><i class="halflings-icon edit"></i><span class="break"></span>AGENT</h2>

           </div>
           <div class="box-content">
           		
               <form  action="" method="POST" class="form-horizontal">
                   <fieldset>
						<?php 
							require_once('class/control_view.php');
							$view = view_agent_by_id($up_id);
						?>
                      <div class="control-group success">
                           <label class="control-label" for="inputSuccess">AGENT NAME</label>
                            <div class="controls">
                               <input type="text" name="up_name"  id="up_name" value="<?php echo $view['agent_name']; ?>">
                               <span class="help-inline"></span>
                           </div>
                       </div>
                       <div class="control-group success">
                           <label class="control-label" for="inputSuccess">PHONE</label>
                            <div class="controls">
                               <input type="text" name="up_phone"  id="up_phone" value="<?php echo $view['phone']; ?>" >
                               <span class="help-inline"></span>
                           </div>
                       </div>
                       <div class="form-actions">
                           <button type="submit" class="btn btn-primary">Save changes</button>
                           <button class="btn">Cancel</button>
                       </div>
                   </fieldset>
               </form>

           </div>

       </div>

   </div>
   <?php include ('footer.php') ?>